﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed = 0.125f;
    public Vector2 offset;
    public float posZ;

    private void LateUpdate()
    {
        Vector2 desiredPosition = new Vector2(target.position.x, target.position.y) + new Vector2 (offset.x,offset.y);
        Vector2 smoothPosition = Vector2.Lerp( new Vector2 (transform.position.x,transform.position.y), desiredPosition, smoothSpeed);
        transform.position = new Vector3 (smoothPosition.x,smoothPosition.y,posZ);
    }
}