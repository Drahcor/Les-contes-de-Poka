﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OeilManager : MonoBehaviour
{
    public GameObject player;
    private GameObject poko;
    private PlayerManager playerManager;

    private float journeyLength;
    private float startTime;

    float posX;
    float posY;
    float posZ;

    public float speed;
    public bool alléRetour;

    //choisir l'axe sur lequel se déplace l'oeil
    public bool deplacementX;
    public float distanceX;

    public bool deplacementY;
    public float distanceY;

    public bool deplacementZ;
    public float distanceZ;

    //état
    public bool alerted;
    public bool follow;
    public bool searching;
    public bool returnPos;


    public float timer;

    void Start()
    {
        posX = transform.position.x;
        posY = transform.position.y;
        posZ = transform.position.z;

        player = GameObject.FindGameObjectWithTag("Player");
        playerManager = player.gameObject.GetComponent<PlayerManager>();
        poko = transform.parent.gameObject;
    }

    void Update()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fractionOfJourney = distCovered / journeyLength;

        Deplacement();
        Regard();
        Searching();
        Alerted();
    }

    void Regard()
    {
        //Raycast du Poko vers un GameObject appelé oeil. Si le joueur est pris dans le raycast il est détecter par le Poko
        RaycastHit hit;
        Debug.DrawLine(new Vector3(transform.position.x , transform.position.y, transform.position.z), poko.transform.position, Color.red);

        if (Physics.Linecast(transform.position, poko.transform.position, out hit))
        {
            Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.CompareTag("Player") && playerManager.isMoving && playerManager.cacher)
            {
                follow = true;
            }
            else if(hit.collider.gameObject.CompareTag("Player") && !playerManager.cacher)
            {
                alerted = true;
            }
        }
    }
    void Alerted()
    {
        if (alerted)
        {
            transform.position = player.transform.position;
        }
    }
    void Deplacement()
    {
        // déplacmeent sur l'axe des X
        if (!alerted && !returnPos)
        {
            if (deplacementX)
            {
                if (transform.position.x <= distanceX + posX && alléRetour)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x + 2, transform.position.y, transform.position.z), speed * Time.deltaTime);
                    alléRetour = true;
                }
                else
                {
                    alléRetour = false;
                }
                if (transform.position.x >= posX - distanceX && !alléRetour)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x - 2, transform.position.y, transform.position.z), speed * Time.deltaTime);
                    alléRetour = false;
                }
                else
                {
                    alléRetour = true;
                }
            }

            // déplacmeent sur l'axe des Z
            if (deplacementZ)
            {
                if (transform.position.z <= distanceZ + posZ && alléRetour)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, transform.position.z + 2), speed * Time.deltaTime);
                    alléRetour = true;
                }
                else
                {
                    alléRetour = false;
                }
                if (transform.position.z >= posZ - distanceZ && !alléRetour)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, transform.position.z - 2), speed * Time.deltaTime);
                    alléRetour = false;
                }
                else
                {
                    alléRetour = true;
                }
            }

            // déplacmeent sur l'axe des Y
            if (deplacementY)
            {
                if (transform.position.y <= distanceY + posY && alléRetour)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y + 2, transform.position.z), speed * Time.deltaTime);
                    alléRetour = true;
                }
                else
                {
                    alléRetour = false;
                }
                if (transform.position.y >= posY - distanceY && !alléRetour)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y - 2, transform.position.z), speed * Time.deltaTime);
                    alléRetour = false;
                }
                else
                {
                    alléRetour = true;
                }
            }
        }
    }
    void Searching()
    {
        // le Poko a repérer quelque chose qui bouge et le suit du regard
        if (follow)
        {
            transform.position = player.transform.position;
            searching = true;
        }
        else
        {
            timer = 0;
        }

        // le joueur est suivis de regard et arrete de bouger
        if (searching || !playerManager.isMoving)
        {
            timer += Time.deltaTime;

            // si le joueur ne bouge plus pendant 3s le poko ne le suit plus
            if(timer >= 3f)
            {
                returnPos = true;
                follow = false;
                searching = false;
            }
        }
        //retourne a la position de départ ( sinon l'oeil conserve le X ou le Y du joueur quand il l'a vu) PS : ne marche que pour les sentinelles
        if (returnPos && transform.parent.CompareTag("sentinelle"))
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(posX, posY, posZ), speed * Time.deltaTime * 2);

            if (transform.position == new Vector3(posX, posY, posZ))
            {
                returnPos = false;
            }
        }
    }
}