﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokoChasseur : MonoBehaviour
{
    private GameObject oeil;
    private OeilManager oeilManager;

    public GameObject[] waypoint;
    private int indexWaypoint;
    private float WPradius = 1;

    public bool gardeAlerte;
    public bool isWaiting;
    public float timerAttente;
    public float speed;

    void Start()
    {
        oeil = gameObject.transform.GetChild(0).gameObject;
        oeilManager = oeil.GetComponent<OeilManager>();
    }

    void Update()
    {
        DeplacementWP();
        Alerted();

        if (waypoint[indexWaypoint].transform.position.x <= transform.position.x)
        {
            transform.rotation = new Quaternion(transform.rotation.x,0, transform.rotation.z,transform.rotation.w);
        }
        else
        {
            transform.rotation = new Quaternion(transform.rotation.x, 180, transform.rotation.z, transform.rotation.w);
        }
    }

    //suis les WP
    private void DeplacementWP()
    {
        if (!gardeAlerte && !isWaiting) // si n'a pas repérer le joueur (ne cherche pas le joueur && n'a pas vus Poka)
        {
            timerAttente = 0;

            if (Vector3.Distance(waypoint[indexWaypoint].transform.position, transform.position) < WPradius) // va au prochain waypoint
            {
                indexWaypoint++;

                // re boucle
                if (indexWaypoint >= waypoint.Length)
                {
                    indexWaypoint = 0;
                }
            }
            // déplacement vers un waypoint
            transform.position = Vector3.MoveTowards(transform.position, waypoint[indexWaypoint].transform.position, Time.deltaTime * speed);
        }
    }

    void Alerted()
    {
        if (oeilManager.alerted)
        {
            transform.position = Vector3.MoveTowards(transform.position, oeilManager.player.transform.position, 0.1f /2);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            oeilManager.player.gameObject.SetActive(false);
        }
    }
}