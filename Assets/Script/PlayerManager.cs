﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public CharacterController controller;
    Vector3 velocity;
    private float gravity = -9.81f;
    public LayerMask groundMask;
    public LayerMask defaultMask;

    public float speed;
    private int startSpeed = 2;

    public bool isGrounded;
    public bool cacher;
    public bool isMoving;

    private float groundDistance = 0.5f;
    private float jumpHeight = 0.5f;


    private void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(vertical, 0f, horizontal).normalized;
        controller.Move(direction * speed * Time.deltaTime);

        //gravité
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        //bloquer le joueur quand il va trop loin
        RaycastHit hit;
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.5f), transform.TransformDirection(Vector3.down), Color.red);
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.5f), transform.TransformDirection(Vector3.down),Color.blue);
        Debug.DrawRay(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), transform.TransformDirection(Vector3.down), Color.white);
        Debug.DrawRay(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), transform.TransformDirection(Vector3.down), Color.green);

        speed = startSpeed;
        //appuie sur Z
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.5f), transform.TransformDirection(Vector3.down), out hit, 2, defaultMask))
        {
            speed = 0;

            if (Input.GetKey("d") || Input.GetKey("s") || Input.GetKey("q"))
            {
                speed = 2;
            }
            if(Input.GetKey("d") && Input.GetKey("z"))
            {
                speed = 0;
            }
        }
        //appuie sur S
        else if(Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.5f), transform.TransformDirection(Vector3.down), out hit, 2, defaultMask))
        {
            speed = 0;

            if (Input.GetKey("q") || Input.GetKey("d") || Input.GetKey("z"))
            {
                speed = 2;
            }
            if (Input.GetKey("s") && Input.GetKey("z"))
            {
                speed = 0;
            }
            else if (Input.GetKey("s") && Input.GetKey("z"))
            {
                speed = 0;
            }
        }
        //appuie sur D
        else if (Physics.Raycast(new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), transform.TransformDirection(Vector3.down), out hit, 2, defaultMask))
        {
            speed = 0;

            if (Input.GetKey("z") || Input.GetKey("s") || Input.GetKey("q"))
            {
                speed = 2;
            }
            if (Input.GetKey("d") && Input.GetKey("z"))
            {
                speed = 0;
            }
            else if (Input.GetKey("d") && Input.GetKey("z"))
            {
                speed = 0;
            }
        }
        //appuie sur Q
        else if (Physics.Raycast(new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), transform.TransformDirection(Vector3.down), out hit, 2, defaultMask))
        {
            speed = 0;

            if (Input.GetKey("d") || Input.GetKey("z") || Input.GetKey("q"))
            {
                speed = 2;
            }
            if (Input.GetKey("q") && Input.GetKey("z"))
            {
                speed = 0;
            }
            else if (Input.GetKey("s") && Input.GetKey("q"))
            {
                speed = 0;
            }
        }
        //sprint
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed *= 2;
        }

        // touche le sol
        isGrounded = Physics.CheckSphere(new Vector3(transform.position.x, transform.position.y - 0.7f, transform.position.z), groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        //jump
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity * speed/2);
        }

       //player is moving
       if(controller.velocity.magnitude > 0)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }
    }

    // se cacher
    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.CompareTag("Cachette"))
        {
            cacher = true;
            startSpeed /= 2;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Cachette"))
        {
            cacher = false;
            startSpeed *= 2;
        }
    }
}